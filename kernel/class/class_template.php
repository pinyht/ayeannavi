<?php
/*
   ------------------------------------------------------------
   作者: pinyht
   编写日期: 2012-01-12
   类说明:
        Smarty模板的扩展函数,用于设定一些基本的配置
   ------------------------------------------------------------
 */

// 引入标准的Smarty类
require_once(dirname(__FILE__).'/../Smarty/Smarty.class.php');

class template extends Smarty
{
    /*
     * 类构造方法
     * 方法说明: 对创建的模板类进行初始化设置
     * 参数说明: 
     *      $templateDir -- 模板路径
     *      $configDir -- 模板配置路径
     *      $template_cDir -- 模板编译路径
     *      $cacheDir -- 模板缓存路径
     */
    public function __construct($templatePath,$configDir,$template_cDir,
            $cacheDir){
        // 初始化各个路径
        $this -> template_dir = $templatePath;
        $this -> config_dir = $configDir;
        // 判断模板编译路径是否存在
        if(is_dir($template_cDir)){
            $this -> compile_dir = $template_cDir;
        }
        else{
           if(mkdir($template_cDir,777)){
               $this -> compile_dir = $template_cDir;
           }
           else{
               exit('创建模板编译目录失败');
           }
        }
        // 判断缓存目录是否存在
        if(is_dir($cacheDir)){
            $this -> cache_dir = $cacheDir;
        }
        else{
            if(mkdir($cacheDir,777)){
                $this -> cache_dir = $cacheDir;
            }
            else{
                exit('创建缓存目录失败');
            }
        }
    }

    /*
     * 选择模板风格方法
     * 方法说明: 选择模板的风格,指定风格的路径
     * 参数说明:
     *      $themeDir -- 风格主题路径,实际调用模板路径为 模板路径/主题路径
     */
    public function SelectTheme($themeDir){
        $this -> template_dir = $templatePath.$themeDir;    // 选择模板
    }

    /*
     * 设置自定义的模板变量标记
     * 方法说明： 使用自己定义的模板标记
     * 参数说明：
     *    $leftDelimiter -- 左边的标记
     *    $rightDelimiter -- 右边的标记
     */
    public function SetDelimiter($leftDelimiter,$rightDelimiter){
        $this -> left_delimiter = $leftDelimiter;
        $this -> right_delimiter = $rightDelimiter;
    }

    /*
     * 设置是否开启debug模式
     * 参数说明:
     *    $debugState -- 需要设置的状态,值为bool类型,true或者false
     */
    public function SetDebugMode($debugState){
        $this -> debugging = $debugState;
    }

    /*
     * 设置是否开启缓存模式
     * 参数说明:
     *    $cacheState -- 需要设置的状态,值为bool类型,true或者false
     */
    public function SetCacheMode($cacheState){
        $this -> caching = $cacheState;
    }
}
?>
