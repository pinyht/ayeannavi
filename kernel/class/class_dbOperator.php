<?php
/*
   ------------------------------------------------------------
   作者: pinyht
   编写日期: 2011-12-02
   功能说明: 数据库相关操作类,包含两个类,dbOperator和dbQuery
   类说明: 
       dbOperator -- 创建一个数据库连接资源,以及数据库相关的基本操作,拥有
                     基本的发送SQL语句的方法Query和能够对敏感字符串进行过
                     滤的EscapeQuery方法,需要跟dbQuery类关联起来使用,在进
                     行query操作后,需要调用dbQuery类来生成返回一个数据操
                     作资源
       dbQuery -- 处理数据库操作资源的类,提供一些方法方便对SQL语句的执行进
                  行处理,该类是一个私有的类,只能由dbOperator类的对象内部自
                  动创建,不允许由程序外部调用
   使用示例:     
       $db = new dbOperator('mysql','localhost','root','12345');
       $db->SelectDatabase('db_test');
       $query = $db->Query('SELECT * FROM tb_table where id=1');
       $rows = $query->ResultRows();   // 返回查找结果的行数
   ------------------------------------------------------------
 */

// 数据库操作类
class dbOperator
{
    // 定义支持的数据库类型
    private $supportDbType = array(
        'mysql'
    );
    private $dbType;    // 数据库类型
    private $server;    // 服务器名,也可以加上端口号,例如'locahost:port'
    private $username;    // 数据库连接用户名,默认服务器进程所有者的用户名
    private $password;    // 密码,默认为空
    private $dbName;    // 数据库名
    private $connection;    // 数据库连接资源
    // 错误消息
    private $errorMessage = array(
            'UnknownDbType' => '未知的数据库类型',
            'CreateConnectionFaild' => '创建数据连接对象失败,请
                                        检查参数是否正确',
            'ConnectFaild' => '连接数据库失败',
            'ErrorMethod' => '方法不存在',
            'SelectDatabaseFaild' => '选择数据库失败',
            'QueryFaild' => '查询失败',
            );
    // 支持的外部调用方法名
    private $supportCommonMethod = array(
            'InitConnection',    // 初始化数据库连接资源
            'CloseConnection',    // 关闭数据库连接资源
            'SelectDatabase',    // 选择要操作的数据库
            'Query',    // 创建一次查询,返回查询结果的资源
            'EscapeQuery',    // 创建一次经过转义的查询,返回查询结果的资源
            );

    // 构造函数,在参数检查没问题后调用初始化方法
    function __construct($dbType,$server,$username,$password){
        if($dbType && $server && $username && $password){
            if(in_array($dbType,$this->supportDbType)){    
                $this->dbType = $dbType;
                // 初始化数据连接,通过通用方法名进行重载
                $this->InitConnection($server,$username,$password);    
            }
            else
                // 返回数据库不支持的错误
                die($this->errorMessage['UnknownDbType']);    
        }
        else
            die($this->errorMessage['CreateConnectionFaild']);
    }
    // 析构函数,在结束数据库操作后调用
    function __destruct(){
        $this->CloseConnection();    // 关闭数据库连接资源
    }
    // 重载方法,所有方法都通过重载来实现,外部调用只需要统一的方法名,便于
    // 扩展支持的数据库类型
    public function __call($name,$arguments){
        // 记录实际需要调用的方法名,通过可变函数来实际调用
        $funcName = '';    
        if(in_array($name,$this->supportCommonMethod)){    
            // 通过数据库类型的判断来重新定义调用的方法
            switch($this->dbType){
                case 'mysql':
                    $funcName = 'mysql' . $name;
            }
            // 以可变函数的形式调用实际的方法,参数为重载方法接收到
            // 的参数数组
            return $this->$funcName($arguments);    
        }
        else
            die($name . $this->errorMessage['ErrorMethod']);
    }

    /*
     * mysql数据库相关的方法
     */

    // 创建数据库连接资源
    private function mysqlInitConnection($arguments){
        // 需要的参数有服务器名,用户名,密码3个参数,用判断参数个数
        // 的方法来假设参数是否匹配
        if(count($arguments) == 3){    
            $this->server = $arguments[0];
            $this->username = $arguments[1];
            $this->password = $arguments[2];

            // 创建mysql连接资源
            $connection = mysql_connect(
                    $this->server,$this->username,$this->password
                    );   
            if(!$connection)    die($this->errorMessage['ConnectFaild']);
            // 设置数据库连接的字符编码为UTF8
            mysql_query('SET NAMES UTF8');    
            $this->connection = $connection;
        }
        else
            die($this->errorMessage['CreateConnectionFaild']);
    }
    // 关闭数据库连接资源
    private function mysqlCloseConnection(){
        if($this->connection)    
            mysql_close($this->connection);
    }
    // 选择要操作的数据库
    private function mysqlSelectDatabase($arguments){
        if(count($arguments) == 1){    
            if(mysql_select_db($arguments[0])){ 
                $this->dbName = $arguments[0];
            }
            else
                die($this->errorMessage['SelectDatabaseFaild']);
        }
        else
            die($this->errorMessage['SelectDatabaseFaild']);
    }
    // 发送一条mysql语句
    private function mysqlQuery($arguments){
        if(count($arguments) == 1){
            $sql = $arguments[0];
            $query = mysql_query($sql,$this->connection);
            if($query){ 
                $result = new dbQuery($this->dbType,$query);
                return $result;
            }
            else
                die(mysql_error());
        }
        else{
            die($this->errorMessage['QueryFaild']);
        }
    }
    // 发送一条经过转义处理的mysql语句
    private function mysqlEscapeQuery($arguments){
        if(count($arguments) == 1){
            $sql = '';      // 需要处理的sql语句
            // 对sql语句进行转义处理
            $sql = $this->CustomEscape($arguments[0],$this->connection);
            $query = mysql_query($sql,$this->connection);
            if($query){ 
                $result = new dbQuery($this->dbType,$query);
                return $result;
            }
            else
                die(mysql_error());
        }
        else{
            die($this->errorMessage['QueryFaild']);
        }
    }
    // 对数组或者字符串进行转义处理,需要mysql连接资源
    private function CustomEscape($data,$resource){
        $dataType = gettype($data);     // 获取输入参数的数据类型
        if($dataType == 'string'){
            $data = $this->EscapeString($data,$resource);
        }
        return $data;
    }

    // 检测服务器是否已经开启magic_quotes_gpc
    private function GetMagic_quote_gpc(){
        if(get_magic_quotes_gpc()){
            return true;
        }
        else{
            return false;
        }
    }

    // 处理字符串的转义
    private function EscapeString($string,$resource){
        // 检测服务器是否已经开启magic_quotes_gpc
        $openMagic_quote_gpc = $this->GetMagic_quote_gpc();    
        $newString = null;      // 转义之后的字符串
        if($openMagic_quote_gpc){
            // 取消系统对字符串的转义
            $newString = stripslashes($string);     
            // 对字符串重新进行转义,该函数会增加对十六进制编码等特殊
            // 情况的考虑
            $newString = mysql_real_escape_string($newString,$resource);
            return $newString;
        }
        else{
            $newString = $string;
            $newString = mysql_real_escape_string($newString,$resource);
            return $newString;
        }
    }
}

// 数据库查询结果操作类
class dbQuery{
    // 定义支持的数据库类型
    private $supportDbType = array(
            'mysql'
            );
    private $dbType;    // 数据库类型
    private $query;    // 数据库查询结果
    // 错误消息
    private $errorMessage = array(
            'UnknownDbType' => '未知的数据库类型',
            'ErrorMethod' => '方法不存在',
            );
    // 支持的外部调用方法名
    private $supportCommonMethod = array(
            'InitQuery',    // 初始化查询结果
            'FetchObject',    // 以对象的形式返回第一条查找结果
            'ResultRows',    // 返回查询结果的行数
            );

    // 构造函数
    function __construct($dbType,$query){
        if($dbType && $query){
            if(in_array($dbType,$this->supportDbType)){
                $this->dbType = $dbType;
                $this->InitQuery($query);
            }
            else{
                die($this->errorMessage['UnknownDbType']);
            }
        }
    }
    // 重载方法,所有方法都通过重载来实现,外部调用只需要统一的方法名,便于
    // 扩展支持的数据库类型
    public function __call($name,$arguments){
        // 记录实际需要调用的方法名,通过可变函数来实际调用
        $funcName = '';    
        if(!in_array($name,$this->supportCommonMethod))  
            die($name . $this->errorMessage['ErrorMethod']);
        // 通过数据库类型的判断来重新定义调用的方法
        switch($this->dbType){
            case 'mysql':
                $funcName = 'mysql' . $name;
        }
        // 以可变函数的形式调用实际的方法,参数为重载方法接收到的参数数组
        return $this->$funcName($arguments);    
    }

    /*
     * mysql数据库相关方法
     */

    // 初始化函数
    private function mysqlInitQuery($aruguments){
        if(count($aruguments) == 1){
            $this->query = $aruguments[0];
        }
    }
    // 以对象的形式返回第一条结果数据
    private function mysqlFetchObject(){
        if($this->query){
            $result =  mysql_fetch_object($this->query);            
            return $result;
        }
    }
    // 执行mysql查询,以整型返回查询到的行数
    private function mysqlResultRows(){
        if($this->query){
            $result = mysql_num_rows($this->query);
            return $result;
        }
    }
}
?>
