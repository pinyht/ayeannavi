<?php
/*
   ------------------------------------------------------------
   作者: pinyht
   编写日期: 2011-12-13
   功能说明: 公共函数库,包括最常用的函数
   ------------------------------------------------------------
 */

/*
 * 对字符串进行HTML代码转义
 * 函数说明: 对接受的字符串进行处理,将HTML标签进行HTML实体的转义,
 *           htmlentities函数的功能是转换字符为HTML字符编码
 * 参数说明: 
 *     $string -- 需要转义的字符串
 */
function EscapeHtml($string){
    // 参数说明:
    //     ENT_QUOTES -- 对单引号和双引号都进行编码
    //     UTF-8 -- 设置字符串设置为UTF-8编码,避免中文乱码
    return htmlentities($string,ENT_QUOTES,'UTF-8'); 
}

/*
 * 对字符串进行URL编码
 * 函数说明: 对接受的字符串进行处理,转换URL编码的形式,增加安全性,
 *           urlencode函数的功能是转换字符为HTML字符编码
 * 参数说明: 
 *     $string -- 需要编码的字符串
 */
function EncodeUrl($string){
    return urlencode($string);
}

/*
 * 对字符串进行URL解码
 * 函数说明: 对接受的字符串进行处理,将URL编码进行解码,方便程序处理
 *           urldecode函数的功能是转换字符为HTML字符编码
 * 参数说明: 
 *     $string -- 需要编码的字符串
 */
function DecodeUrl($string){
    return urldecode($string);
}

?>
