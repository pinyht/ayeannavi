<?php
/*
   ------------------------------------------------------------
   作者: pinyht
   编写日期: 2011-12-13
   功能说明: 系统配置文件,系统相关的基础配置在此进行定义
   ------------------------------------------------------------
 */

   
/*
 * 设置系统错误报告
 * 函数说明: ini_set函数用来设定系统环境的参数
 * 参数说明:
 *      error_reporting -- 设置错误报告的级别
 *      E_ALL -- E_STRICT出外的所有错误和警告信息         
 *      E_STRICT -- 启用PHP对代码的修改建议,以确保代码具有最佳的互操作性
 *                  和向前兼容性
 *      E_NOTICE -- 运行时通知,表示脚本遇到可能会表现为错误的情况,但是在
 *                  可以正常运行的脚本里面也可能会有类似的通知
 */

ini_set('error_reporting',E_ALL);
//跟踪除注意信息之外的所有错误
//ini_set('error_reporting',E_ALL & ~E_NOTICE);  

/*
 * 关闭屏幕显示错误
 * 参数说明:
 *      display_errors -- 该选项设置是否将错误信息作为输出的一部分显示
 *                        到屏幕,或者对用户隐藏而不显示,该配置值的类型
 *                        为boolean
 */

ini_set('display_errors',false);

/*
 * 开启错误日志
 * 参数说明:
 *      log_errors -- 设置是否将脚本运行的错误信息记录到服务器错误日志或
 *                    者error_log之中,该配置值的类型为boolean
 */

ini_set('log_errors',true);

/*
 * 设置所有脚本的输出编码为utf-8
 * 函数说明: header函数用来发送一个自定义的HTTP头信息
 */

header('Content-Type: text/html; charset=utf-8');

/*
 * 取得站点根目录的绝对路径
 * 函数说明: dirname -- 返回路径中的目录部分
 * 参数说明: __FILE__ -- 文件的完整路径和文件名,魔术常量
 */

define('ROOT_PATH',dirname(__FILE__));

/*
 * 定义模板相关路径
 */

define('TEMPLATE_PATH',ROOT_PATH.'/template/');     // 模板主路径
define('TEMPLATE_THEME_PATH',TEMPLATE_PATH.'theme/');   // 模板主题路径
define('TEMPLATE_CONFIG_PATH',TEMPLATE_PATH.'config'); // 模板配置路径
define('TEMPLATE_COMPILE_PATH',TEMPLATE_PATH.'compile');   // 模板编译路径
define('TEMPLATE_CACHE_PATH',TEMPLATE_PATH.'cache');   // 模板缓存路径

/*
 * 初始化模板对象
 */
$template = new template(TEMPLATE_THEME_PATH,TEMPLATE_CONFIG_PATH,
        TEMPLATE_COMPILE_PATH,TEMPLATE_CACHE_PATH);
// 选择模板主题为default,对应default路径中的文件
$template -> SelectTheme('default');    
$template -> SetDelimiter('<{','}>');    // 设置模板变量左右结束标记 
$template -> SetDebugMode(false);   // 关闭debug模式
$template -> SetCacheMode(false);   // 关闭缓存模式

?>
