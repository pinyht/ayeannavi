-- ------------------------------------------------------------
-- 作者: pinyht
-- 编写日期: 2011-11-24
-- 功能说明: 自动创建导航系统数据库的SQL脚本
-- ------------------------------------------------------------

-- ----------------------------------------------------------
-- 创建数据库 db_ayeannavi
-- ----------------------------------------------------------
-- 判断是否已经存在db_ayeannavi这个数据库,存在就先删除掉
DROP DATABASE IF EXISTS db_ayeannavi;	
CREATE DATABASE db_ayeannavi		-- 创建数据库
DEFAULT CHARACTER SET utf8;	-- 设置默认字符集为utf8
USE db_ayeannavi;	     		-- 进入数据库

-- ----------------------------------------------------------
-- 创建用户表 tb_user
-- ----------------------------------------------------------
DROP TABLE IF EXISTS tb_user;		-- 判断是否存在表tb_user,存在就先删除这个表
CREATE TABLE tb_user (
        -- 创建userid字段,非空,自增,从1开始,主键
        userid bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,	
        username char(20) NOT NULL UNIQUE,	      -- 唯一约束
        password char(40) NOT NULL,
        email varchar(50) NOT NULL UNIQUE,
        passprompt varchar(100) NULL,
        promptanswer varchar(80) NULL
        );

-- ----------------------------------------------------------
-- 创建管理员表 tb_admin
-- ----------------------------------------------------------
DROP TABLE IF EXISTS tb_admin;
CREATE TABLE tb_admin (
        adminid smallint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        username char(20) NOT NULL UNIQUE,
        password varchar(40) NOT NULL,
        email varchar(50) NOT NULL UNIQUE
        );

-- ----------------------------------------------------------
-- 创建导航定制表 tb_navicustom
-- ----------------------------------------------------------
DROP TABLE IF EXISTS tb_navicustom;
CREATE TABLE tb_navicustom (
        navicusid bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        userid bigint unsigned NOT NULL,
        title varchar(30) NOT NULL,
        background varchar(50) NOT NULL
        );

-- ----------------------------------------------------------
-- 创建分页内容表 tb_page
-- ----------------------------------------------------------
DROP TABLE IF EXISTS tb_page;
CREATE TABLE tb_page (
        pageid bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        navicusid bigint unsigned NOT NULL,
        name varchar(30) NOT NULL,
        level tinyint unsigned NOT NULL,
        fontcolor char(10) NOT NULL
        );

-- ----------------------------------------------------------
-- 创建分类表 tb_category
-- ----------------------------------------------------------
DROP TABLE IF EXISTS tb_category;
CREATE TABLE tb_category (
        categoryid bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        pageid bigint unsigned NOT NULL,
        userid bigint unsigned NOT NULL,
        name varchar(30) NOT NULL,
        fontcolor char(10) NOT NULL,
        isbold bit NOT NULL
        );

-- ----------------------------------------------------------
-- 创建网址内容表 tb_site
-- ----------------------------------------------------------
DROP TABLE IF EXISTS tb_site;
CREATE TABLE tb_site (
        siteid bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        categoryid bigint unsigned NOT NULL,
        userid bigint unsigned NOT NULL,
        name varchar(30) NOT NULL,
        address varchar(300) NOT NULL,
        description varchar(80) NULL
        );
